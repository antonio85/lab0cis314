
import java.util.Scanner;                         //import Scanner package
 
public class lab0 {
 public static int gcd(int a, int b){
  // Find the greatest common divisor of a and b
  // Hint: Use Euclids Algorithm
     int bigger = (a>b)? a:b;; //set up the variables
     int smaller = (a<b)?a:b;;
     int gcd=0;
             
     do{     
         if(bigger%smaller==0){
             return smaller;//exit the function 
         }
         gcd = bigger%smaller;
         bigger = smaller;
            
         smaller = gcd;
          
     }while(true);     
      
 }
 
 public static int lcm(int a, int b){
  // Find the least common multiple of a and b
  // Hint: Use the gcd of a and b
    return (a*b)/gcd(a,b);
   
   
 }
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);
    int numProblems = scanner.nextInt();          //retrieve the number of lines
   
    for(int i = 0; i < numProblems; ++i){
      int a = scanner.nextInt();                  //retrieve the first integer
      int b = scanner.nextInt();                  //retrieve the second integer
  
      int g = gcd(a,b);
      int l = lcm(a,b);
  
 
 
        System.out.print(g + " ");
        System.out.println(l);
    }
 
    scanner.close();
 }
 
}
